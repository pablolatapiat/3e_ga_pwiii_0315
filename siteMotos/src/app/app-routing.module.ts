import { CadastroClientesComponent } from './components/cadastro-clientes/cadastro-clientes.component';
import { CustomComponent } from './components/custom/custom.component';
import { ScooterComponent } from './components/scooter/scooter.component';
import { SportComponent } from './components/sport/sport.component';
import { StreetComponent } from './components/street/street.component';
import { TouringComponent } from './components/touring/touring.component';
import { TrailComponent } from './components/trail/trail.component';
import { LoginComponent } from './components/login/login.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'login', component: LoginComponent},
  {path: 'cadastro-clientes', component: CadastroClientesComponent},
  {path: 'trail', component: TrailComponent},
  {path: 'touring', component: TouringComponent},
  {path: 'street', component: StreetComponent},
  {path: 'sport', component: SportComponent},
  {path: 'scooter', component: ScooterComponent},
  {path: 'custom', component: CustomComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-trail',
  templateUrl: './trail.component.html',
  styleUrls: ['./trail.component.css']
})
export class TrailComponent implements OnInit {

  darkMode: boolean = false

  constructor() { }

  ngOnInit(): void {
  }

}

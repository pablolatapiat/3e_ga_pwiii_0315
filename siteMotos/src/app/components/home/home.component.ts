import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  darkMode:boolean = false
  imgIndex: number = 1

  constructor() { }

  ngOnInit(): void {
    setInterval(() => {this.imgIndex == 3 ? this.imgIndex = 1 : this.imgIndex ++}, 4000)
  }

}

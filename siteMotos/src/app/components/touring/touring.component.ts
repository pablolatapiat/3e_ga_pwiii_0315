import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-touring',
  templateUrl: './touring.component.html',
  styleUrls: ['./touring.component.css']
})
export class TouringComponent implements OnInit {

  darkMode: boolean = false

  constructor() { }

  ngOnInit(): void {
  }

}

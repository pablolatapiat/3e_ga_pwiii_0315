import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  darkMode:boolean = false
  chevState:boolean = false
  folderCategorias: boolean = false

  constructor() { }

  ngOnInit(): void {
  }

}
